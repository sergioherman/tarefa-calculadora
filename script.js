var btnAdd = document.querySelector(".btnAdd")
var txtArea = document.querySelector(".textNotas")
var inputNota = document.querySelector(".inputNota")

let textoNota = ''
let vetorNotas = []
let nn = 1
btnAdd.addEventListener("click", () => {
    num = inputNota.value
    num = num.replace(",",".")
    let testaNum = !(isNaN(parseFloat(num)))
    if (num >= 0 && num<=10 && num != '' && testaNum == true){
        textoNota += ('A nota ')+ nn + (' foi ') + inputNota.value.trim() + ('\n')
        vetorNotas.push(parseFloat(num))
        nn++
    }else if (inputNota.value/*.trim()*/ == ''){
        alert('Por favor, insira uma nota.')
    }else{
        alert('A nota digitada é inválida, por favor, insira uma nota válida.')
    }
    txtArea.innerHTML = (textoNota)
    inputNota.value = ''
})


var result = document.querySelector(".resultado")
var btnCalcMed = document.querySelector(".btnCalc")
btnCalcMed.addEventListener("click", () => {
    let media = 0
    for (k in vetorNotas){
        media += vetorNotas[k]
    }
    media = media / vetorNotas.length
    if (isNaN(media) == false){
        let txtResult = ('A média é: ') + media.toFixed(2)
        result.innerText = txtResult 
    }
})
